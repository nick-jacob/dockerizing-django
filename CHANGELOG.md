# Dockerizing Django

## Unreleased

### Added
  + Added changelog.
  + Added contribution guide.
  + Added initial project files for a basic django to-do app.
  + Added a root level project folder named `img`.
  + Added a `.env` file that includes some initial global variables.
  
  