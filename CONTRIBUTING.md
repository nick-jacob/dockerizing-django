## Start Project
* `docker-compose build`
* `docker-compose up -d`
* http://localhost:80


## Common Commands
- Creating a python virtual environment
  * `mkvirtualenv --python=/Users/jacob/.pyenv/versions/3.4.3/bin/python3 dockerizing-django`


## Common Database Commands
- Connect to a database via Postgres Shell
  * `psql -h 127.0.0.1 -p 5432 -U postgres --password`


## Common Docker Commands
- Create database migrations
  * `docker-compose run web python manage.py migrate`

- List Containers
  * `docker-compose ps`
  * `docker-compose ps -q` (list full length container ids)

- List environment variables available to a service
  * `docker-compose run <service-name> env`

- Logs
  * `docker-compose logs`
